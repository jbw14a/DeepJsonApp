//
//  DetailViewController.swift
//  jsonApp
//
//  Created by John Wolfe on 4/23/18.
//  Copyright © 2018 jbw14a. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearsLabel: UILabel!
    @IBOutlet weak var formatLabel: UILabel!
    @IBOutlet weak var episodesLabel: UILabel!
    @IBOutlet weak var networkLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    
    @IBOutlet weak var showImageView: UIImageView!
    @IBOutlet weak var castButton: UIButton!
    
    var thisShow: ShowModel?
    {
        didSet {
            // Update the veiw
            configureView()
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    
    
    func configureView() {
        // Update the user interface for the detail item.
        
        if let unwrappedShow = thisShow {
            
            titleLabel?.text = unwrappedShow.title
            
            if (unwrappedShow.start != "" && unwrappedShow.end != ""){
                yearsLabel?.text = unwrappedShow.start + "  -  " + unwrappedShow.end
            } else if (unwrappedShow.end == ""){
                yearsLabel?.text = unwrappedShow.start
            }
                
            formatLabel?.text = unwrappedShow.format
            
            if (unwrappedShow.numEpisodes != "0" || unwrappedShow.numEpisodes != " "){
                episodesLabel?.text = unwrappedShow.numEpisodes + " episodes"
            }
            
            networkLabel?.text = unwrappedShow.network
            descriptionLabel?.text = unwrappedShow.descriptionSkrt
            descriptionTextView?.text = unwrappedShow.summary

            let url = URL(string: unwrappedShow.imageUrl)
            DispatchQueue.global().async {
                //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    self.showImageView.image = UIImage(data: data!)
                }
            }
            
            
            
        } else {
            
            print("Nothing happened")
            
            
        }
        
    }
    


}

