//
//  MasterViewController.swift
//  jsonApp
//
//  Created by John Wolfe on 4/23/18.
//  Copyright © 2018 jbw14a. All rights reserved.
//
// Links that saved my life:
//
// https://www.simplifiedios.net/swift-json-tutorial/
// https://gitlab.com/CS315_2017/swift_tables
//



import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var objects = [Any]()
    
    let urlShows = "https://api.myjson.com/bins/1ahrbf"
    
    var franchiseNames: Array<String> = []
    var showsArray: Array<ShowModel> = []
    
    var fullArray: Array<Array<ShowModel>> = []
    
    
    
    func parseJson(){
        let url = NSURL(string: urlShows)
        
        URLSession.shared.dataTask(with: (url as URL?)!, completionHandler: { (data, response, error) -> Void in
            if let jsonObject1 = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary {
                if let datDict = jsonObject1!.value(forKey: "datData") as? NSDictionary {
                    if let franchiseArray = datDict.value(forKey: "franchise") as? NSArray {
                        
                        var tempArray: Array<ShowModel> = []
                        for franchise in franchiseArray {
                            if let franchiseDict = franchise as? NSDictionary {
                                if let franchiseName = franchiseDict.value(forKey: "franchiseName"){
                                    self.franchiseNames.append((franchiseName as? String)!)
                                }
                                
                                if let entriesArray = franchiseDict.value(forKey: "entries") as? NSArray{
                                    for entry in entriesArray {
                                        if let singleEntry = entry as? NSDictionary {
                                
                                            var showNameRip = ""
                                            var showFormatRip = ""
                                            var showStartRip = ""
                                            var showEndRip = ""
                                            var showEpisodesRip = ""
                                            var showNetworkRip = ""
                                            var showImageUrlRip = ""
                                            var showDescriptionRip = ""
                                            var showSummaryRip = ""
                                            var showCharactersRip: Array<String> = []
                                            var showCastRip: Array<String> = []
                                            
                                            if let name = singleEntry.value(forKey: "name"){
                                                showNameRip = name as! String
                                                
                                            }
                                            if let format = singleEntry.value(forKey: "format"){
                                                showFormatRip = format as! String
                                                
                                            }
                                            if let yearStart = singleEntry.value(forKey: "yearStart"){
                                                showStartRip = yearStart as! String
                                                
                                            }
                                            if let yearEnd = singleEntry.value(forKey: "yearEnd"){
                                                showEndRip = yearEnd as! String
                                                
                                            }
                                            if let episodes = singleEntry.value(forKey: "episodes"){
                                                let episodesInt = episodes as! Int
                                                showEpisodesRip = String(episodesInt)
                                                
                                            }
                                            if let network = singleEntry.value(forKey: "network"){
                                                showNetworkRip = network as! String
                                                
                                            }
                                            if let imageUrl = singleEntry.value(forKey: "imageURL"){
                                                showImageUrlRip = imageUrl as! String
                                                
                                            }
                                            if let description = singleEntry.value(forKey: "description"){
                                                showDescriptionRip = description as! String
                                                
                                            }
                                            if let summary = singleEntry.value(forKey: "summary"){
                                                showSummaryRip = summary as! String
                                                
                                            }
                                            
                                            if let starring = singleEntry.value(forKey: "name") as? NSArray {
                                                
                                                for star in starring {
                                                    if let starDict = star as? NSDictionary {
                                                        
                                                        if let characterSkrtBoi = starDict.value(forKey: "playing"){
                                                            showCharactersRip.append((characterSkrtBoi as? String)!)
                                                        }
                                                        
                                                        if let cast = starDict.value(forKey: "name"){
                                                            showCastRip.append((cast as? String)!)
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            let show = ShowModel(showTitle: showNameRip, showFormat: showFormatRip, showStart: showStartRip, showEnd: showEndRip, numOfEpisodes: showEpisodesRip, showNetwork: showNetworkRip, showImageUrl: showImageUrlRip, showDescription: showDescriptionRip, showSummary: showSummaryRip, showCharacters: showCharactersRip, showCast: showCastRip)
                                            self.showsArray.append(show)
                                            //print(show.title + " " + show.numEpisodes + " " + show.end )
                                            
                                            tempArray.append(show)
                                        }
                                    }
                                }
                            }
                        }
                        self.fullArray.append(tempArray)
                    }
                }
            }
        }).resume()
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //My Code
        parseJson()
        
        // Set navigation Image
        let irdbImage = UIImage(named: "irdb.png")
        let myImageView = UIImageView(image: irdbImage!)
        self.navigationItem.titleView = myImageView
        
        
        
        //Boiler plate code
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    
    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return fullArray.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let thisList = fullArray[section]
        return thisList.count
    }

    
    
    
    
    // I need to pass Show Objects to each of these table cells.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        //let object = showsArray[indexPath.row]
        
        let thisSection = indexPath.section
        let thisRow = indexPath.row
        let arrayINeed = fullArray[thisSection]
        let object = arrayINeed[thisRow]
        
        cell.textLabel?.text = object.title
        
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        for x in fullArray {
            print(x[2].title)
        }
        
        
        let headerString = franchiseNames[section]
        
        return headerString
        
    }
    
    

    
    
    // MARK: - Segues
    
    // This is where I want to pass the selected film to the DetailsViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {

                // This is where we call the DetailsViewController
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController

                let thisSection = indexPath.section
                let thisRow = indexPath.row
                let arrayINeed = fullArray[thisSection]
                let selectedShow = arrayINeed[thisRow]
                
                // Set dat object
                //controller.thisShow = showsArray[indexPath.row]
                controller.thisShow = selectedShow
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
        
        //let destinationNav = segue.destination as! UINavigationController
        //let destinationDetailView = destinationNav.viewControllers[0] as! DetailViewController
        
        
        //destinationDetailView.thisShow = selectedShow
        
    }


}

