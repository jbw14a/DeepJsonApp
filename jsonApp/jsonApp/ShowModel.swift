//
//  FranchiseModal.swift
//  jsonApp
//
//  Created by John Wolfe on 4/23/18.
//  Copyright © 2018 jbw14a. All rights reserved.
//

import Foundation
import UIKit

class ShowModel: NSObject {
    
    var title: String = ""
    var format: String = ""
    var start: String = ""
    var end: String = ""
    var numEpisodes: String = ""
    var network: String = ""
    var imageUrl: String = ""
    var descriptionSkrt: String = ""
    var summary: String = ""
    var characters: Array<String> = []
    var cast: Array<String> = []
    
    init(showTitle: String, showFormat: String, showStart: String, showEnd: String, numOfEpisodes: String, showNetwork: String, showImageUrl: String, showDescription: String, showSummary: String, showCharacters: Array<String>, showCast: Array<String>){
        
        title = showTitle
        format = showFormat
        start = showStart
        end = showEnd
        numEpisodes = numOfEpisodes
        network = showNetwork
        imageUrl = showImageUrl
        descriptionSkrt = showDescription
        summary = showSummary
        characters = showCharacters
        cast = showCast
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
}

